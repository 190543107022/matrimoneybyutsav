package com.utsav.matrimony.Adapter;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.utsav.matrimony.Model.LanguageModel;
import com.utsav.matrimony.R;

import java.util.ArrayList;

public class SpLanguageAdpater extends BaseAdapter {

    ArrayList<LanguageModel> languageList;
    Activity context;

    ViewHolder holder;

    public SpLanguageAdpater(ArrayList<LanguageModel> languageList, Activity context) {
        this.languageList = languageList;
        this.context = context;
    }

    @Override
    public int getCount() {
        return languageList.size();
    }

    @Override
    public Object getItem(int position) {
        return languageList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return languageList.get(position).getLanguageId();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        //LayoutInflater
        LayoutInflater layoutInflater = context.getLayoutInflater();

        //ConverView Check
        if(convertView == null){

            //Set Convert View
            convertView = layoutInflater.inflate(R.layout.row_candidate_language, null);

            //Holder Object Initialize
            holder = new SpLanguageAdpater.ViewHolder();

            //TextView Bind
            holder.row_candidate_language_tvLanguageId = convertView.findViewById(R.id.row_candidate_language_tvLanguageId);
            holder.row_candidate_language_tvLanguageName = convertView.findViewById(R.id.row_candidate_language_tvLanguageName);
        }

        //Get Data From List And Set Into TextView
        int languageId = languageList.get(position).getLanguageId();
        String languageName = languageList.get(position).getLanguageName();

        holder.row_candidate_language_tvLanguageId.setText(String.valueOf(languageId));
        holder.row_candidate_language_tvLanguageName.setText(languageName);

        return convertView;
    }

    class ViewHolder{
        //TextView Objects
        TextView row_candidate_language_tvLanguageId,row_candidate_language_tvLanguageName;
    }
}
