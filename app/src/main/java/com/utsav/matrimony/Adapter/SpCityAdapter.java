package com.utsav.matrimony.Adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.utsav.matrimony.Model.CityModel;
import com.utsav.matrimony.R;

import java.util.ArrayList;

public class SpCityAdapter extends BaseAdapter {

    ArrayList<CityModel> cityList;
    Activity context;

    ViewHolder holder;

    public SpCityAdapter(ArrayList<CityModel> cityList, Activity context) {
        this.cityList = cityList;
        this.context =  context;
    }

    @Override
    public int getCount() {
        return cityList.size();
    }

    @Override
    public Object getItem(int position) {
        return cityList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return cityList.get(position).getCityId();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        //LayoutInflater Object Initialize
        LayoutInflater layoutInflater = context.getLayoutInflater();

        //Check ConvertView
        if (convertView == null) {

            //Which Layout Use To Set Values
            convertView = layoutInflater.inflate(R.layout.row_candidate_city, null);

            //Holder Initialize
            holder = new SpCityAdapter.ViewHolder();

            //TextView Initialize
            holder.row_candidate_city_tvCityId = convertView.findViewById(R.id.row_candidate_city_tvCityId);
            holder.row_candidate_city_tvCityName = convertView.findViewById(R.id.row_candidate_city_tvCityName);

            //Set ConvertView Tag
            convertView.setTag(holder);
        }else {
            //If ConvertView Set That Direct Use
            holder = (SpCityAdapter.ViewHolder) convertView.getTag();
        }

        //Get City Id From List
        int cityId = cityList.get(position).getCityId();
        String cityName = cityList.get(position).getCityName();

        holder.row_candidate_city_tvCityId.setText(String.valueOf(cityId));
        holder.row_candidate_city_tvCityName.setText(cityName);

        //Return View
        return convertView;

    }

    public class ViewHolder{

        //TextView Object
        TextView row_candidate_city_tvCityId,row_candidate_city_tvCityName;
    }

}
