package com.utsav.matrimony.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.utsav.matrimony.Database.TblCandidate;
import com.utsav.matrimony.Model.CandidateModel;
import com.utsav.matrimony.R;

import java.util.ArrayList;

public class CandidateListAdapter extends RecyclerView.Adapter<CandidateListAdapter.ViewHolder> {

    //ArrayList Of Candidate
    ArrayList<CandidateModel> candidateList;

    //Context Of Activity
    Context context;

    //TblCandidate Object
    TblCandidate tblCandidate;

    public CandidateListAdapter(ArrayList<CandidateModel> candidateList, Context context) {
        this.candidateList = candidateList;
        this.context = context;
        tblCandidate = new TblCandidate(context);
    }

    @NonNull
    @Override
    public CandidateListAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        //Attach Our Row File With ViewHolder
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_candidate_list, parent, false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull CandidateListAdapter.ViewHolder holder, int position) {

        //Display Name
        holder.row_candidate_list_tvName.setText(candidateList.get(position).getName());

        //Display Email
        holder.row_candidate_list_tvEmail.setText(candidateList.get(position).getEmail());

        //Display DOB
        holder.row_candidate_list_tvDOB.setText(candidateList.get(position).getDob());

        //Favorite Is 0 or 1
        if (candidateList.get(position).getIsFavorite() == 0) {
            //If IsFavorite Is 0 That UnFavorite Data
            holder.row_candidate_list_ivFavoriteCandidate.setImageResource(R.drawable.ic_unfavorite);
        } else {
            //Is IsFavorite Is 1 That Favorite Data
            holder.row_candidate_list_ivFavoriteCandidate.setImageResource(R.drawable.ic_favorite);
        }

        //Image Click Event
        //Favorite Click
        holder.row_candidate_list_ivFavoriteCandidate.setOnClickListener(v -> {

            //TblCandidate Favorite Value Changed
            tblCandidate.changeFavorite(candidateList.get(position).getCandidateId(),candidateList.get(position).getIsFavorite() == 0 ? 1 : 0);

            //Candidate List IsFavorite Data Change
            candidateList.get(position).setIsFavorite(candidateList.get(position).getIsFavorite() == 0 ? 1 : 0);

            //Adapter Reset
            notifyDataSetChanged();

        });

        //Delete Click
        holder.row_candidate_list_ivDeleteCandidate.setOnClickListener(v -> {

            //TblCandidate Remove Candidate Details
            tblCandidate.deleteCandidateRecord(candidateList.get(position).getCandidateId());

            //Remove From List
            candidateList.remove(candidateList.get(position));

            //Adapter Reset
            notifyItemRemoved(position);
            notifyDataSetChanged();

        });

    }

    @Override
    public int getItemCount() {
        return candidateList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        //TextView and Button Object
        TextView row_candidate_list_tvName, row_candidate_list_tvEmail, row_candidate_list_tvDOB,activity_list_candidate_tvNoDataFound;

        ImageView row_candidate_list_ivFavoriteCandidate, row_candidate_list_ivDeleteCandidate;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            //Bind All Object With Id
            //TextView
            row_candidate_list_tvName = itemView.findViewById(R.id.row_candidate_list_tvName);
            row_candidate_list_tvEmail = itemView.findViewById(R.id.row_candidate_list_tvEmail);
            row_candidate_list_tvDOB = itemView.findViewById(R.id.row_candidate_list_tvDOB);
            activity_list_candidate_tvNoDataFound = itemView.findViewById(R.id.activity_list_candidate_tvNoDataFound);

            //Image View
            row_candidate_list_ivFavoriteCandidate = itemView.findViewById(R.id.row_candidate_list_ivFavoriteCandidate);
            row_candidate_list_ivDeleteCandidate = itemView.findViewById(R.id.row_candidate_list_ivDeleteCandidate);

        }
    }
}
