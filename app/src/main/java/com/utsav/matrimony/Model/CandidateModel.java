package com.utsav.matrimony.Model;

import java.io.Serializable;

public class CandidateModel implements Serializable {

    int CandidateId;
    String Name;
    String SurName;
    String FatherName;
    int Gender;
    int CityId;
    int LanguageId;
    String Email;
    String Mobile;
    String Hobbies;
    int IsFavorite;
    String DOB;

    public int getCandidateId() {
        return CandidateId;
    }

    public void setCandidateId(int candidateId) {
        CandidateId = candidateId;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getSurName() {
        return SurName;
    }

    public void setSurName(String surName) {
        SurName = surName;
    }

    public String getFatherName() {
        return FatherName;
    }

    public void setFatherName(String fatherName) {
        FatherName = fatherName;
    }

    public int getGender() {
        return Gender;
    }

    public void setGender(int gender) {
        Gender = gender;
    }

    public int getCityId() {
        return CityId;
    }

    public void setCityId(int cityId) {
        CityId = cityId;
    }

    public int getLanguageId() {
        return LanguageId;
    }

    public void setLanguageId(int languageId) {
        LanguageId = languageId;
    }

    public String getEmail() {
        return Email;
    }

    public void setEmail(String email) {
        Email = email;
    }

    public String getMobile() {
        return Mobile;
    }

    public void setMobile(String mobile) {
        Mobile = mobile;
    }

    public String getHobbies() {
        return Hobbies;
    }

    public void setHobbies(String hobbies) {
        Hobbies = hobbies;
    }

    public int getIsFavorite() {
        return IsFavorite;
    }

    public void setIsFavorite(int isFavorite) {
        IsFavorite = isFavorite;
    }

    public String getDob() {
        return DOB;
    }

    public void setDob(String DOB) {
        this.DOB = DOB;
    }

    @Override
    public String toString() {
        return "CandidateModel{" +
                "CandidateId=" + CandidateId +
                ", Name='" + Name + '\'' +
                ", SurName='" + SurName + '\'' +
                ", FatherName='" + FatherName + '\'' +
                ", Gender=" + Gender +
                ", CityId=" + CityId +
                ", LanguageId=" + LanguageId +
                ", Email='" + Email + '\'' +
                ", Mobile='" + Mobile + '\'' +
                ", Hobbies='" + Hobbies + '\'' +
                ", IsFavorite=" + IsFavorite +
                ", DOB='" + DOB + '\'' +
                '}';
    }
}
