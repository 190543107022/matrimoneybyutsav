package com.utsav.matrimony.Database;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.utsav.matrimony.Model.CityModel;
import com.utsav.matrimony.Model.LanguageModel;

import java.util.ArrayList;

public class TblCity extends DatabaseHelper{

    //Table Name
    final static String TBL_CITY = "TblCity";

    //Columns Names
    final static String COL_TBL_CITY_CITYID = "CityId";
    final static String COL_TBL_CITY_CITYNAME = "CityName";

    //Get Data From Database
    public ArrayList<CityModel> all_city_list(){

        //Sqlite Database Object
        SQLiteDatabase db = getReadableDatabase();

        //Create Array List Empty
        ArrayList<CityModel> cityList = new ArrayList<>();

        //Sql Statement
        String query = "select * from " + TBL_CITY;

        //Get Data From Database Into Cursor
        //db.rawQuery For Select Query Into Database
        Cursor cursor = db.rawQuery(query,null);

        //Loop For Cursor Start To End The All Data

        //Cursor Reset Our Postion
        cursor.moveToFirst();

        //First To Last Loop
        do{

            //City Model Is Empty
            CityModel cityModel  = new CityModel();

            //Cursor Data Set Into CityModel
            cityModel.setCityId(cursor.getInt(cursor.getColumnIndex(COL_TBL_CITY_CITYID)));
            cityModel.setCityName(cursor.getString(cursor.getColumnIndex(COL_TBL_CITY_CITYNAME)));

            //Add Model Into List
            cityList.add(cityModel);

        }while(cursor.moveToNext());


        System.out.println("City List Count "+cityList.size());

        //Database Close
        db.close();
        //Cursor Close
        cursor.close();

        //Return City List
        return cityList;
    }


    public TblCity(Context context) {
        super(context);
    }
}
