package com.utsav.matrimony.Database;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.utsav.matrimony.Model.LanguageModel;

import java.util.ArrayList;

public class TblLanguage extends DatabaseHelper{

    //Table Name
    final static String TBL_LANGUAGE = "TblLanguage";

    //Columns Names
    final static String COL_TBL_LANGUAGE_LANGUAGEID = "LanguageId";
    final static String COL_TBL_LANGUAGE_LANGUAGENAME = "LanguageName";


    //Get Data From Database
    public ArrayList<LanguageModel> all_language_list(){

        //Sqlite Database Object
        SQLiteDatabase db = getReadableDatabase();

        //Create Array List Empty
        ArrayList<LanguageModel> languageList = new ArrayList<>();

        //Sql Statement
        String query = "select * from " + TBL_LANGUAGE;

        //Get Data From Database Into Cursor
        //db.rawQuery For Select Query Into Database
        Cursor cursor = db.rawQuery(query,null);

        //Loop For Cursor Start To End The All Data

        //Cursor Reset Our Postion
        cursor.moveToFirst();

        //First To Last Loop
        do{

            //Language Model Empty
            LanguageModel languageModel  = new LanguageModel();

            //Set Language Model
            languageModel.setLanguageId(cursor.getInt(cursor.getColumnIndex(COL_TBL_LANGUAGE_LANGUAGEID)));
            languageModel.setLanguageName(cursor.getString(cursor.getColumnIndex(COL_TBL_LANGUAGE_LANGUAGENAME)));

            //Add Model Into Array List
            languageList.add(languageModel);

        }while (cursor.moveToNext());

        //Database Close
        db.close();
        //Cursor Close
        cursor.close();

        //Return Language List
        return languageList;
    }



    public TblLanguage(Context context) {
        super(context);
    }
}
