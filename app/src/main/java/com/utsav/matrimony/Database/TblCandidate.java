package com.utsav.matrimony.Database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.utsav.matrimony.Model.CandidateModel;

import java.util.ArrayList;

public class TblCandidate extends DatabaseHelper{

    //Table Name
    public static final String TBL_CANDIDATE = "TblCandidate";

    //Column Names
    public static final String COL_TBL_CANDIDATE_CANDIDATEID = "CandidateId";
    public static final String COL_TBL_CANDIDATE_NAME = "Name";
    public static final String COL_TBL_CANDIDATE_FATHERNAME = "FatherName";
    public static final String COL_TBL_CANDIDATE_SURNAME = "SurName";
    public static final String COL_TBL_CANDIDATE_GENDER = "Gender";
    public static final String COL_TBL_CANDIDATE_CITYID = "CityId";
    public static final String COL_TBL_CANDIDATE_LANGUAGEID = "LanguageId";
    public static final String COL_TBL_CANDIDATE_EMAIL = "Email";
    public static final String COL_TBL_CANDIDATE_MOBILE = "Mobile";
    public static final String COL_TBL_CANDIDATE_HOBBIES = "Hobbies";
    public static final String COL_TBL_CANDIDATE_ISFAVORITE = "IsFavorite";
    public static final String COL_TBL_CANDIDATE_DOB = "DOB";

    //Select For db.rawQuery
    //Update For db.update
    //Insert For db.insert
    //Delete For db.delete

    public TblCandidate(Context context) {
        super(context);
    }

    //Insert Into Database
    public long insertCandidateRecord(CandidateModel candidateModel){

        //Insert / Update / Modify / Delete That Use getWritableDatabase()
        //Select / View That Use getReadableDatabase()

        //Sqlite Database Object
        SQLiteDatabase db = getWritableDatabase();

        //Content Values Blank
        ContentValues cv = new ContentValues();

        cv.put(COL_TBL_CANDIDATE_NAME,candidateModel.getName());
        cv.put(COL_TBL_CANDIDATE_FATHERNAME,candidateModel.getFatherName());
        cv.put(COL_TBL_CANDIDATE_SURNAME,candidateModel.getSurName());
        cv.put(COL_TBL_CANDIDATE_GENDER,candidateModel.getGender());
        cv.put(COL_TBL_CANDIDATE_EMAIL,candidateModel.getEmail());
        cv.put(COL_TBL_CANDIDATE_CITYID,candidateModel.getCityId());
        cv.put(COL_TBL_CANDIDATE_LANGUAGEID,candidateModel.getLanguageId());
        cv.put(COL_TBL_CANDIDATE_HOBBIES,candidateModel.getHobbies());
        cv.put(COL_TBL_CANDIDATE_ISFAVORITE,candidateModel.getIsFavorite());
        cv.put(COL_TBL_CANDIDATE_DOB,candidateModel.getDob());
        cv.put(COL_TBL_CANDIDATE_MOBILE,candidateModel.getMobile());

        //Insert Record Into Datbase
        long inserted = db.insert(TBL_CANDIDATE,null,cv);

        //long update = db.update(TBL_CANDIDATE,cv,""+COL_TBL_CANDIDATE_CANDIDATEID+" = ?",new String[]{String.valueOf(candidateModel.getCandidateId())});

        //Return If Inserted 1 Otherwise 0 Or Less
        return inserted;

    }

    //Select TblCandidate Table
    public ArrayList<CandidateModel> all_candidate_list(){

        //Sqlite Database Object
        SQLiteDatabase db = getReadableDatabase();

        //Create Array List Empty
        ArrayList<CandidateModel> candidateList = new ArrayList<>();

        //Sql Statement
        String query = "select * from "+TBL_CANDIDATE;

        //Get Data Into Cursor
        Cursor cursor = db.rawQuery(query,null);

        //Reset Position Of Cursor
        cursor.moveToFirst();

        if(cursor.getCount()>0) {
            do {

                //CandidateModel Empty
                CandidateModel candidateModel = new CandidateModel();

                //Set Data Into Model
                candidateModel.setCandidateId(cursor.getInt(cursor.getColumnIndex(COL_TBL_CANDIDATE_CANDIDATEID)));
                candidateModel.setName(cursor.getString(cursor.getColumnIndex(COL_TBL_CANDIDATE_NAME)));
                candidateModel.setSurName(cursor.getString(cursor.getColumnIndex(COL_TBL_CANDIDATE_SURNAME)));
                candidateModel.setFatherName(cursor.getString(cursor.getColumnIndex(COL_TBL_CANDIDATE_FATHERNAME)));
                candidateModel.setGender(cursor.getInt(cursor.getColumnIndex(COL_TBL_CANDIDATE_GENDER)));
                candidateModel.setCityId(cursor.getInt(cursor.getColumnIndex(COL_TBL_CANDIDATE_CITYID)));
                candidateModel.setLanguageId(cursor.getInt(cursor.getColumnIndex(COL_TBL_CANDIDATE_LANGUAGEID)));
                candidateModel.setEmail(cursor.getString(cursor.getColumnIndex(COL_TBL_CANDIDATE_EMAIL)));
                candidateModel.setMobile(cursor.getString(cursor.getColumnIndex(COL_TBL_CANDIDATE_MOBILE)));
                candidateModel.setHobbies(cursor.getString(cursor.getColumnIndex(COL_TBL_CANDIDATE_HOBBIES)));
                candidateModel.setIsFavorite(cursor.getInt(cursor.getColumnIndex(COL_TBL_CANDIDATE_ISFAVORITE)));
                candidateModel.setDob(cursor.getString(cursor.getColumnIndex(COL_TBL_CANDIDATE_DOB)));

                //Candiate Model Add Into List
                candidateList.add(candidateModel);

            } while (cursor.moveToNext());
        }
        //Database Close
        db.close();

        //Cursor Close
        cursor.close();

        //Return Candidate List

        return candidateList;
    }

    //Change IsFavoriteValue Into TblCandidate Table
    public void changeFavorite(int candidateId,int favoriteValue){

        //Sqlite database Object
        SQLiteDatabase db = getWritableDatabase();

        //Sql Statement
        String query = "Update "+TBL_CANDIDATE+" SET "+COL_TBL_CANDIDATE_ISFAVORITE+" = "+favoriteValue+" where "+COL_TBL_CANDIDATE_CANDIDATEID+" = "+candidateId;

        //Run Sql Query
        db.execSQL(query);

        //Database Close
        db.close();

    }

    //Delete Candidate Details
    public void deleteCandidateRecord(int candidateId){

        //Sqlite Database Object
        SQLiteDatabase db = getWritableDatabase();

        //Sql Statement
        String query = "delete from "+TBL_CANDIDATE+" where "+COL_TBL_CANDIDATE_CANDIDATEID+" = "+candidateId;

        //Execute Query
        db.execSQL(query);

        //Database Close
        db.close();
    }

    //Select TblCandidate Table From FavoriteRecord
    public ArrayList<CandidateModel> all_candidate_favorite_list(){

        //Sqlite Database Object
        SQLiteDatabase db = getReadableDatabase();

        //Create Array List Empty
        ArrayList<CandidateModel> candidateList = new ArrayList<>();

        //Sql Statement
        String query = "select * from "+TBL_CANDIDATE + " where "+COL_TBL_CANDIDATE_ISFAVORITE+" = 1";

        //Get Data Into Cursor
        Cursor cursor = db.rawQuery(query,null);

        //Reset Position Of Cursor
        cursor.moveToFirst();

        if(cursor.getCount()>0) {
            do {

                //CandidateModel Empty
                CandidateModel candidateModel = new CandidateModel();

                //Set Data Into Model
                candidateModel.setCandidateId(cursor.getInt(cursor.getColumnIndex(COL_TBL_CANDIDATE_CANDIDATEID)));
                candidateModel.setName(cursor.getString(cursor.getColumnIndex(COL_TBL_CANDIDATE_NAME)));
                candidateModel.setSurName(cursor.getString(cursor.getColumnIndex(COL_TBL_CANDIDATE_SURNAME)));
                candidateModel.setFatherName(cursor.getString(cursor.getColumnIndex(COL_TBL_CANDIDATE_FATHERNAME)));
                candidateModel.setGender(cursor.getInt(cursor.getColumnIndex(COL_TBL_CANDIDATE_GENDER)));
                candidateModel.setCityId(cursor.getInt(cursor.getColumnIndex(COL_TBL_CANDIDATE_CITYID)));
                candidateModel.setLanguageId(cursor.getInt(cursor.getColumnIndex(COL_TBL_CANDIDATE_LANGUAGEID)));
                candidateModel.setEmail(cursor.getString(cursor.getColumnIndex(COL_TBL_CANDIDATE_EMAIL)));
                candidateModel.setMobile(cursor.getString(cursor.getColumnIndex(COL_TBL_CANDIDATE_MOBILE)));
                candidateModel.setHobbies(cursor.getString(cursor.getColumnIndex(COL_TBL_CANDIDATE_HOBBIES)));
                candidateModel.setIsFavorite(cursor.getInt(cursor.getColumnIndex(COL_TBL_CANDIDATE_ISFAVORITE)));
                candidateModel.setDob(cursor.getString(cursor.getColumnIndex(COL_TBL_CANDIDATE_DOB)));

                //Candiate Model Add Into List
                candidateList.add(candidateModel);

            } while (cursor.moveToNext());
        }
        //Database Close
        db.close();

        //Cursor Close
        cursor.close();

        //Return Candidate List

        return candidateList;
    }

    //Search Data From List
    //Select TblCandidate Table
    public ArrayList<CandidateModel> all_search_list(String data){

        //Sqlite Database Object
        SQLiteDatabase db = getReadableDatabase();

        //Create Array List Empty
        ArrayList<CandidateModel> candidateList = new ArrayList<>();

        //Sql Statement
        String query = "select * from "+TBL_CANDIDATE+" where "+COL_TBL_CANDIDATE_NAME+" LIKE '%"+data+"%'";

        //Get Data Into Cursor
        Cursor cursor = db.rawQuery(query,null);

        //Reset Position Of Cursor
        cursor.moveToFirst();

        if(cursor.getCount()>0) {
            do {

                //CandidateModel Empty
                CandidateModel candidateModel = new CandidateModel();

                //Set Data Into Model
                candidateModel.setCandidateId(cursor.getInt(cursor.getColumnIndex(COL_TBL_CANDIDATE_CANDIDATEID)));
                candidateModel.setName(cursor.getString(cursor.getColumnIndex(COL_TBL_CANDIDATE_NAME)));
                candidateModel.setSurName(cursor.getString(cursor.getColumnIndex(COL_TBL_CANDIDATE_SURNAME)));
                candidateModel.setFatherName(cursor.getString(cursor.getColumnIndex(COL_TBL_CANDIDATE_FATHERNAME)));
                candidateModel.setGender(cursor.getInt(cursor.getColumnIndex(COL_TBL_CANDIDATE_GENDER)));
                candidateModel.setCityId(cursor.getInt(cursor.getColumnIndex(COL_TBL_CANDIDATE_CITYID)));
                candidateModel.setLanguageId(cursor.getInt(cursor.getColumnIndex(COL_TBL_CANDIDATE_LANGUAGEID)));
                candidateModel.setEmail(cursor.getString(cursor.getColumnIndex(COL_TBL_CANDIDATE_EMAIL)));
                candidateModel.setMobile(cursor.getString(cursor.getColumnIndex(COL_TBL_CANDIDATE_MOBILE)));
                candidateModel.setHobbies(cursor.getString(cursor.getColumnIndex(COL_TBL_CANDIDATE_HOBBIES)));
                candidateModel.setIsFavorite(cursor.getInt(cursor.getColumnIndex(COL_TBL_CANDIDATE_ISFAVORITE)));
                candidateModel.setDob(cursor.getString(cursor.getColumnIndex(COL_TBL_CANDIDATE_DOB)));

                //Candiate Model Add Into List
                candidateList.add(candidateModel);

            } while (cursor.moveToNext());
        }
        //Database Close
        db.close();

        //Cursor Close
        cursor.close();

        //Return Candidate List

        return candidateList;
    }

}
