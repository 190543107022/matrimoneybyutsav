package com.utsav.matrimony.UI;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.utsav.matrimony.Adapter.CandidateListAdapter;
import com.utsav.matrimony.Database.TblCandidate;
import com.utsav.matrimony.Model.CandidateModel;
import com.utsav.matrimony.R;

import java.util.ArrayList;

public class SearchActivity extends AppCompatActivity {

    //ArrayList Of Candidate
    ArrayList<CandidateModel> candidateList;

    //TblCandidate Object
    TblCandidate tblCandidate;

    //CandidateListAdapter
    CandidateListAdapter adapter;

    //TextView Object
    TextView activity_search_tvNoDataFound;

    //EditText Object
    EditText activity_search_etSearch;

    //Recycler View Object
    RecyclerView activity_search_rvSearchList;

    String data;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);

        init();
        process();
        listeners();

    }

    private void init() {

        //TextView Initialize
        activity_search_tvNoDataFound = findViewById(R.id.activity_search_tvNoDataFound);

        //EditText Initialize
        activity_search_etSearch = findViewById(R.id.activity_search_etSearch);

        //Recycler View Initialize
        activity_search_rvSearchList = findViewById(R.id.activity_search_rvSearchList);

        //Candidate ArrayList Empty
        candidateList = new ArrayList<>();

        //TblCandidate Initialize
        tblCandidate = new TblCandidate(this);

        //Get Data From Database And Set Into ArrayList
        candidateList = tblCandidate.all_candidate_list();


    }

    private void process() {

        checkDataIsEmpty();


    }

    private void listeners() {

        activity_search_etSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                data = s.toString();
                getDataFromDatabase();
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

    }


    //Recycler View Complete Reset / Set
    void setAdapter() {

        //Recycler View Layout Manager Set
        activity_search_rvSearchList.setLayoutManager(new GridLayoutManager(this, 1));

        //Adapter Set
        adapter = new CandidateListAdapter(candidateList, this);

        //Recycler View Adapter Set
        activity_search_rvSearchList.setAdapter(adapter);
    }

    private void getDataFromDatabase() {
        //Array Data Get From Database
        candidateList = tblCandidate.all_search_list(data);

        checkDataIsEmpty();
    }

    void checkDataIsEmpty() {
        if (candidateList.size() < 1) {
            activity_search_tvNoDataFound.setVisibility(View.VISIBLE);
            activity_search_rvSearchList.setVisibility(View.GONE);
        } else {

            activity_search_tvNoDataFound.setVisibility(View.GONE);
            activity_search_rvSearchList.setVisibility(View.VISIBLE);

            setAdapter();

        }
    }

}