package com.utsav.matrimony.UI;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.Toast;

import com.utsav.matrimony.Adapter.SpCityAdapter;
import com.utsav.matrimony.Adapter.SpLanguageAdpater;
import com.utsav.matrimony.Database.TblCandidate;
import com.utsav.matrimony.Database.TblCity;
import com.utsav.matrimony.Database.TblLanguage;
import com.utsav.matrimony.Model.CandidateModel;
import com.utsav.matrimony.Model.CityModel;
import com.utsav.matrimony.Model.LanguageModel;
import com.utsav.matrimony.R;
import com.utsav.matrimony.Utils.Constant;

import java.util.ArrayList;

public class AddCandidateActivity extends AppCompatActivity {


    //Edit Text Object For Get User Input
    EditText activity_add_candidate_etName, activity_add_candidate_etFatherName, activity_add_candidate_etSurName, activity_add_candidate_etDOB, activity_add_candidate_etEmail, activity_add_candidate_etMobile;

    //Spinner For Select City / Language
    Spinner activity_add_candidate_spCity,activity_add_candidate_spLanguage;

    //Radio Button For Gender
    RadioButton activity_add_candidate_rbMale, activity_add_candidate_rbFemale;

    //Button
    Button activity_add_candidate_btnSubmit;

    //Radio Group
    RadioGroup activity_add_candidate_rgGender;

    //Check Box For Hobbies
    CheckBox activity_add_candidate_chbCricket,activity_add_candidate_chbFootball,activity_add_candidate_chbDance;

    //ArrayList String For Checkbox
    ArrayList<String> checkboxSelected;

    //Temp Variable For Get Data From User
    String name, fatherName, surName,DOB,email,mobile,hobbies;
    int spCityPosition,spLanguagePosition;
    boolean rbMale;

    //Table Candidate Object
    TblCandidate tblCandidate;

    //Candidate Model Object
    CandidateModel candidateModel;

    //CityAdapter Object
    SpCityAdapter cityAdapter;

    //Tbl City Object
    TblCity tblCity;

    //City ArrayList
    ArrayList<CityModel> cityList;

    //Language Arraylist For Get From Database
    ArrayList<LanguageModel> languageList;

    //SpLanguageAdapter Object
    SpLanguageAdpater languageAdapter;

    //TblLanguage Object
    TblLanguage tblLanguage;

    //Constant Object
    Constant constant;

    //Check Inserted Successfully
    long insertedCheck;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_candidate);

        init();
        process();
        listeners();

    }

    private void init() {

        //Find View By Id
        //EditText
        activity_add_candidate_etName = findViewById(R.id.activity_add_candidate_etName);
        activity_add_candidate_etFatherName = findViewById(R.id.activity_add_candidate_etFatherName);
        activity_add_candidate_etSurName = findViewById(R.id.activity_add_candidate_etSurName);
        activity_add_candidate_etDOB = findViewById(R.id.activity_add_candidate_etDOB);
        activity_add_candidate_etEmail = findViewById(R.id.activity_add_candidate_etEmail);
        activity_add_candidate_etMobile = findViewById(R.id.activity_add_candidate_etMobile);


        //Radio Group
        activity_add_candidate_rgGender = findViewById(R.id.activity_add_candidate_rgGender);

        //Check Box
        activity_add_candidate_chbCricket = findViewById(R.id.activity_add_candidate_chbCricket);
        activity_add_candidate_chbFootball = findViewById(R.id.activity_add_candidate_chbFootball);
        activity_add_candidate_chbDance = findViewById(R.id.activity_add_candidate_chbDance);

        //Spinner
        activity_add_candidate_spCity = findViewById(R.id.activity_add_candidate_spCity);
        activity_add_candidate_spLanguage = findViewById(R.id.activity_add_candidate_spLanguage);

        //Radio Button
        activity_add_candidate_rbMale = findViewById(R.id.activity_add_candidate_rbMale);
        activity_add_candidate_rbFemale = findViewById(R.id.activity_add_candidate_rbFemale);

        //Button Click
        activity_add_candidate_btnSubmit = findViewById(R.id.activity_add_candidate_btnSubmit);

        //Checkbox Array List
        checkboxSelected = new ArrayList<>();

        //Blank City List
        cityList = new ArrayList<>();

        //Blank Language List
        languageList = new ArrayList<>();

        //Candidate Model Initialize Empty
        candidateModel = new CandidateModel();

        //Table Candidate Object Initialize
        tblCandidate = new TblCandidate(this);

        //Table Language Object Initialize
        tblLanguage = new TblLanguage(this);

        //Table City Object Initialize
        tblCity = new TblCity(this);

        //Get Data TblCity and Set Into List
        cityList = tblCity.all_city_list();

        //Get Data TblLanguage And Set Into List
        languageList = tblLanguage.all_language_list();

        constant = new Constant();

    }

    private void process() {

        /*//Default Focus On Name Field
        activity_add_candidate_etName.requestFocus();*/

        //Set City Adapter
        cityAdapter = new SpCityAdapter(cityList,this);

        activity_add_candidate_spCity.setAdapter(cityAdapter);

        //Set Language Adapter
        languageAdapter = new SpLanguageAdpater(languageList,this);
        activity_add_candidate_spLanguage.setAdapter(languageAdapter);


    }

    private void listeners() {


        //Spinner Get Currect Position
        activity_add_candidate_spCity.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                spCityPosition = position;
                //System.out.println("Position " + position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        activity_add_candidate_spLanguage.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                spLanguagePosition = position;
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        activity_add_candidate_btnSubmit.setOnClickListener(v -> {
            if(IsValidData()){

                //Data Set Into Temp Variables
                //getDataFromFields();

                //TempData Set Into Model
                candidateModel.setName(name);
                candidateModel.setFatherName(fatherName);
                candidateModel.setSurName(surName);
                //Male Is 1 and Female Is 2
                candidateModel.setGender(rbMale ? Constant.MALE : Constant.FEMALE);
                candidateModel.setEmail(email);
                candidateModel.setHobbies(hobbies);
                candidateModel.setCityId(cityList.get(spCityPosition).getCityId());
                candidateModel.setLanguageId(languageList.get(spLanguagePosition).getLanguageId());
                candidateModel.setMobile(mobile);
                candidateModel.setDob(DOB);
                candidateModel.setIsFavorite(Constant.ISFAVORITEDEFAULT);

                //Model Pass Into Database
                insertedCheck = tblCandidate.insertCandidateRecord(candidateModel);

                //Check Inserted Or Not
                if(insertedCheck>0){
                    Toast.makeText(this, "Inserted Successfully", Toast.LENGTH_SHORT).show();
                    finish();
                }else{
                    Toast.makeText(this, "SomeThing Went Wrong", Toast.LENGTH_SHORT).show();
                }

            }
        });

    }

    public boolean IsValidData(){
        boolean valid = true;

        getDataFromFields();

        //Name Is Not Empty
        if(TextUtils.isEmpty(name)){
            activity_add_candidate_etName.setError("Please Enter Some Details");
            activity_add_candidate_etName.requestFocus();
            valid = false;
        }

        //Name Is Not Less Than Length 3
        if(name.length()<3){
            activity_add_candidate_etName.setError("Please Enter Some Details");
            activity_add_candidate_etName.requestFocus();
            valid = false;
        }

        //Father Name Is Not Empty
        if(TextUtils.isEmpty(fatherName)){
            activity_add_candidate_etFatherName.setError("Please Enter Some Details");
            activity_add_candidate_etFatherName.requestFocus();
            valid = false;
        }

        //Father Is Not Less Than Length 3
        if(fatherName.length()<3){
            activity_add_candidate_etFatherName.setError("Please Enter Some Details");
            activity_add_candidate_etFatherName.requestFocus();
            valid = false;
        }

        //SurName Is Not Empty
        if(TextUtils.isEmpty(surName)){
            activity_add_candidate_etSurName.setError("Please Enter Some Details");
            activity_add_candidate_etSurName.requestFocus();
            valid = false;
        }

        //SurName Is Not Less Than Length 3
        if(surName.length()<3){
            activity_add_candidate_etSurName.setError("Please Enter Some Details");
            activity_add_candidate_etSurName.requestFocus();
            valid = false;
        }

        //Email Is Not Empty
        if(TextUtils.isEmpty(email)){
            activity_add_candidate_etEmail.setError("Please Enter Some Details");
            activity_add_candidate_etEmail.requestFocus();
            valid = false;
        }

        //Mobile Is Not Empty
        if(TextUtils.isEmpty(mobile)){
            activity_add_candidate_etMobile.setError("Please Enter Some Details");
            activity_add_candidate_etMobile.requestFocus();
            valid = false;
        }

        //Mobile Is Exact 10 Character
        if(mobile.length()!=10){
            activity_add_candidate_etMobile.setError("Please Mobile Number Is 10 Digit");
            activity_add_candidate_etMobile.requestFocus();
            valid = false;
        }

        if(checkboxSelected.size()==0){
            Toast.makeText(this, "Please Select Hobbies", Toast.LENGTH_SHORT).show();
        }

        return valid;

    }

    void getDataFromFields(){

        //Just Get Data And Set Into Temp Variables

        //Edit Text Temp Variable
        name = dataSpacesRemove(activity_add_candidate_etName.getText().toString());
        fatherName = dataSpacesRemove(activity_add_candidate_etFatherName.getText().toString());
        surName = dataSpacesRemove(activity_add_candidate_etSurName.getText().toString());
        email = dataSpacesRemove(activity_add_candidate_etEmail.getText().toString());
        mobile = dataSpacesRemove(activity_add_candidate_etMobile.getText().toString());
        DOB = dataSpacesRemove(activity_add_candidate_etDOB.getText().toString());


        //Set Hobbies
        hobbies = "";
        checkboxSelected = new ArrayList<>();
        if(activity_add_candidate_chbCricket.isChecked()){
            //hobbies += "Cricket ";
            checkboxSelected.add("Cricket");
        }
        if(activity_add_candidate_chbFootball.isChecked()){
            //hobbies += "Football ";
            checkboxSelected.add("Football");
        }
        if(activity_add_candidate_chbDance.isChecked()){
            //hobbies += "Dance ";
            checkboxSelected.add("Dance");
        }
        int i =0;

        for (i = 0;i<checkboxSelected.size();i++){
            if(checkboxSelected.size()>1){
                hobbies += checkboxSelected.get(i) + ",";
            }else{
                hobbies += checkboxSelected.get(i);
            }
        }

        //Toast.makeText(this, "Hobbies : "+hobbies, Toast.LENGTH_SHORT).show();


        //String[] arraySelected = hobbies.split(",");


        //Toast.makeText(this, "Hobbies : "+arraySelected.length, Toast.LENGTH_SHORT).show();

        //RadioButton Temp Variable
        rbMale = activity_add_candidate_rbMale.isChecked();

    }

    String dataSpacesRemove(String s){

        s = s.replace("'","");
        s = s.trim();
        return s;
    }

}