package com.utsav.matrimony.UI;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.utsav.matrimony.R;

public class DashboardActivity extends AppCompatActivity {

    //TextView For Going Into New Activity
    TextView activity_dashboard_tvAddCandidate,activity_dashboard_tvListCandidate,activity_dashboard_tvFavoriteCandidate,activity_dashboard_tvSearchCandidate;

    //Intent Object For Open New Activity
    Intent intent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);

        init();
        process();
        listeners();
    }

    //Initialize Of All Variable And Create Of Null / Empty Objects
    private void init() {

        //TextView Bind With ID
        activity_dashboard_tvAddCandidate = findViewById(R.id.activity_dashboard_tvAddCandidate);
        activity_dashboard_tvListCandidate = findViewById(R.id.activity_dashboard_tvListCandidate);
        activity_dashboard_tvFavoriteCandidate = findViewById(R.id.activity_dashboard_tvFavoriteCandidate);
        activity_dashboard_tvSearchCandidate = findViewById(R.id.activity_dashboard_tvSearchCandidate);

    }

    private void process() {
    }

    private void listeners() {

        //Add Activity On Click
        activity_dashboard_tvAddCandidate.setOnClickListener(v -> {
            intent = new Intent(DashboardActivity.this, AddCandidateActivity.class);
            startActivity(intent);
        });

        //List Activity On CLick
        activity_dashboard_tvListCandidate.setOnClickListener(v -> {
            intent = new Intent(DashboardActivity.this,ListActivity.class);
            startActivity(intent);
        });

        activity_dashboard_tvFavoriteCandidate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                intent = new Intent(DashboardActivity.this,FavoriteActivity.class);
                startActivity(intent);
            }
        });

        //Search Activity Open
        activity_dashboard_tvSearchCandidate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                intent = new Intent(DashboardActivity.this,SearchActivity.class);
                startActivity(intent);
            }
        });

    }
}