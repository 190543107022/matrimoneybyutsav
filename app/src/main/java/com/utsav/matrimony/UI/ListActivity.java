package com.utsav.matrimony.UI;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.utsav.matrimony.Adapter.CandidateListAdapter;
import com.utsav.matrimony.Database.TblCandidate;
import com.utsav.matrimony.Model.CandidateModel;
import com.utsav.matrimony.R;

import java.util.ArrayList;

public class ListActivity extends AppCompatActivity {

    //ArrayList Of Candidate
    ArrayList<CandidateModel> candidateList;

    //TblCandidate Object
    TblCandidate tblCandidate;

    //CandidateList Adapter Object
    CandidateListAdapter adapter;

    //Recycler View Object For Display Candidate List
    RecyclerView activity_list_candidate_rvCandidateList;

    //TextView Object
    TextView activity_list_candidate_tvNoDataFound;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_candidate);

        init();
        process();
        listeners();

    }

    //Initialize Of All Object
    private void init() {

        //Array List Empty
        candidateList = new ArrayList<>();

        //TblCandidate Initialize
        tblCandidate = new TblCandidate(this);

        //Get Data From Database And Set Into Array List
        candidateList = tblCandidate.all_candidate_list();

        //Recycler View Initialize
        activity_list_candidate_rvCandidateList = findViewById(R.id.activity_list_candidate_rvCandidateList);

        //TextView Initialize
        activity_list_candidate_tvNoDataFound = findViewById(R.id.activity_list_candidate_tvNoDataFound);

    }

    private void process() {

        if(candidateList.size()<1){
            activity_list_candidate_tvNoDataFound.setVisibility(View.VISIBLE);
            activity_list_candidate_rvCandidateList.setVisibility(View.GONE);
        }else{

            activity_list_candidate_tvNoDataFound.setVisibility(View.GONE);
            activity_list_candidate_rvCandidateList.setVisibility(View.VISIBLE);

            //Set Layout Manager For Recycler View
            activity_list_candidate_rvCandidateList.setLayoutManager(new GridLayoutManager(this,1));

            //Adapter Set
            adapter = new CandidateListAdapter(candidateList,this);

            //Recycler View Adapter set
            activity_list_candidate_rvCandidateList.setAdapter(adapter);
        }



    }

    private void listeners() {
    }
}