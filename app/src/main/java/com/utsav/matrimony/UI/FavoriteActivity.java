package com.utsav.matrimony.UI;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.utsav.matrimony.Adapter.CandidateListAdapter;
import com.utsav.matrimony.Database.TblCandidate;
import com.utsav.matrimony.Model.CandidateModel;
import com.utsav.matrimony.R;

import java.util.ArrayList;

public class FavoriteActivity extends AppCompatActivity {

    //Candidate ArrayList
    ArrayList<CandidateModel> candidateList;

    //TblCandidate Object
    TblCandidate tblCandidate;

    //CandidateListAdapter Object
    CandidateListAdapter adapter;

    //TextView Object
    TextView activity_favorite_tvNoDataFound;

    //RecyclerView Object
    RecyclerView activity_favorite_rvFavoriteList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_favorite);

        init();
        process();
        listeners();

    }

    private void init() {

        //Candidate ArrayList Empty
        candidateList = new ArrayList<>();

        //TblCandidate Initialize
        tblCandidate = new TblCandidate(this);

        //Get Data From Database And Set Into ArrayList
        candidateList = tblCandidate.all_candidate_favorite_list();

        //TextView Initialize
        activity_favorite_tvNoDataFound = findViewById(R.id.activity_favorite_tvNoDataFound);

        //Recycler View Initialize
        activity_favorite_rvFavoriteList = findViewById(R.id.activity_favorite_rvFavoriteList);

    }

    private void process() {

        if(candidateList.size()<1){
            activity_favorite_tvNoDataFound.setVisibility(View.VISIBLE);
            activity_favorite_rvFavoriteList.setVisibility(View.GONE);
        }else {

            activity_favorite_tvNoDataFound.setVisibility(View.GONE);
            activity_favorite_rvFavoriteList.setVisibility(View.VISIBLE);

            //Recycler View Layout Manager Set
            activity_favorite_rvFavoriteList.setLayoutManager(new GridLayoutManager(this, 1));

            //Adapter Set
            adapter = new CandidateListAdapter(candidateList, this);

            //Recycler View Adapter Set
            activity_favorite_rvFavoriteList.setAdapter(adapter);
        }

    }

    private void listeners() {
    }
}